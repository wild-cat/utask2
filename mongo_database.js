/**
 * Created by Кирилл on 21.07.2016.
 */

const IP = "45.55.239.248";
const PORT = "27017";
const DATABASE_NAME = "uTask";
const MONGO_CLIENT = require("mongodb").MongoClient;

const MongoDatabase = {
    db: null
};

MongoDatabase.findAll = function(collection, filter, callback) {
    if (!this.db) {
        this.connect((err) => {
            if (err) {
                return callback(err);
            }
            this._findAll(collection, filter, callback);
        });
    } else {
        this._findAll(collection, filter, callback);
    }
};

MongoDatabase._findAll = function(collection, filter, callback) {
    this.db.collection(collection).find(filter).toArray(function (err, data) {
        if (err) {
            console.log("Error: couldn't find clients: ", collection, filter);
            callback(err);
            return;
        }
        callback(null, data);
    });
};

MongoDatabase.insertMany = function(collection, data, callback) {
    if (!this.db) {
        this.connect((err) => {
            if (err) {
                return callback(err);
            }
            this._insertMany(collection, data, callback);
        });
    } else {
        this._insertMany(collection, data, callback);
    }
};

MongoDatabase._insertMany = function(collection, data, callback) {
    this.db.collection(collection).insertMany(data, (err) => {
        if (err) {
            console.log("Error: couldn't insert documents into collection ", collection, data);
            callback(err);
            return;
        }
        console.log("Inserted " + data.length + " documents into " + collection);
        callback(null);
    });
};

MongoDatabase.connect = function(callback) {
    var url = "mongodb://" + IP + ":" + PORT + "/" + DATABASE_NAME;
    MONGO_CLIENT.connect(url, (err, db) => {
        if (err) {
            console.log("Error: couldn't connect to database");
            callback(err);
            return;
        }
        this.db = db;
        callback(null, db);
    });
};

MongoDatabase.close = function() {
    this.db.close();
};

exports.MongoDatabase = MongoDatabase;