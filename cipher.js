/**
 * Created by Кирилл on 26.07.2016.
 */

function XOREncrypt(dataToEncrypt, key) {
    var result = "";
    for (var i = 0; i < dataToEncrypt.length; i++)
    {
        result += String.fromCharCode(key.charCodeAt(i % key.length) ^ dataToEncrypt.charCodeAt(i));
    }
    return result;
}

function XORDecrypt(dataToDecrypt, key) {
    var result = "";
    for (var i = 0; i < dataToDecrypt.length; i++)
    {
        result += String.fromCharCode(key.charCodeAt(i % key.length) ^ dataToDecrypt.charCodeAt(i));
    }
    return result;
}

exports.XOREncrypt = XOREncrypt;
exports.XORDecrypt = XORDecrypt;