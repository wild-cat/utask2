/**
 * Created by Кирилл on 25.07.2016.
 */
const PATH_TO_VIEWS = "view/";

const pug = require("pug");
const Util = {};

Util.renderView = function(viewName, data, res) {
    if (!data.msgType) {
        data.msgType = "";
    }
    if (!data.message) {
        data.message = "";
    }
    var viewFullName = (viewName.lastIndexOf(".pug") !== -1) ? PATH_TO_VIEWS + viewName : PATH_TO_VIEWS + viewName + ".pug";
    var fn = pug.compileFile(viewFullName);
    var html = fn({form: data});
    res.send(html);
};

module.exports = Util;