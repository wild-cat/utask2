/**
 * Created by Кирилл on 23.07.2016.
 */
const db = require("../mongo_database").MongoDatabase;
const util = require("../util");
const mailer = require("../mailer");
const encrypt = require("../cipher").XOREncrypt;
const decrypt = require("../cipher").XORDecrypt;

function recoverPhoneAction(req, res, next) {
    console.log("recoverPhoneAction, params:", req.params);

    var email = decodeURIComponent(req.params.email);
    var key = decodeURIComponent(req.params.key);
    var emailEnc = encrypt(email, key);

    db.findAll("clients", {email: emailEnc}, (err, data) => {
        if (err) {
            console.log("Error: when finding a client: ", err.message, err.stack);
            util.renderView("recover_phone", {message: "Ошибка", msgType: "fail"}, res);
            return
        }
        if (data.length === 0) {
            console.log("Warn: client with email '" + email + "' was not found");
            util.renderView("recover_phone", {message: "Клиент с таким email не найден"}, res);
        } else if (data.length > 1) {
            console.log("Warn: more than 1 client associated to email '" + email + "'");
            util.renderView("recover_phone", {message: "! email связан с неск. клиентами !", msgType: "fail"}, res);
        } else {
            console.log("Found phone number hash" + data[0].phone + " for client with e-mail " + email);
            var phoneClean = decrypt(data[0].phone, key);
            var mailOptions = {
                from: '"NodeJS application" <jsnode@rambler.ru>',
                to: email,
                subject: 'Восстановление номера телефона',
                text: 'Здравствуйте! К вашему email привязан номер телефона "' + phoneClean + '"',
                html: 'Здравствуйте! К вашему email привязан номер телефона "' + phoneClean + '"'
            };
            mailer.sendMail(mailOptions, (err) => {
                if (err) {
                    util.renderView("recover_phone", {
                        message: "Невозможно отправить письмо. Попробуйте повторить запрос через 10 минут",
                        msgType: "fail"
                    }, res);
                    return;
                }
                util.renderView("recover_phone", {
                    message: "На ящик '" + email + "' отправлено письмо",
                    msgType: "success"
                }, res);
            });
        }
    });
}

module.exports = recoverPhoneAction;