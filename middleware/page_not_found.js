/**
 * Created by Кирилл on 23.07.2016.
 */
var util = require("../util");

function pageNotFound(req, res, next) {
    util.renderView("page_not_found", {}, res);
}

module.exports = pageNotFound;