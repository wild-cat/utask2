/**
 * Created by Кирилл on 23.07.2016.
 */

exports.addClient = require("./add_client");
exports.recoverPhone = require("./recover_phone");
exports.pageNotFound = require("./page_not_found");

exports.addClientAction = require("./add_client_action");
exports.recoverPhoneAction = require("./recover_phone_action");
