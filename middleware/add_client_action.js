/**
 * Created by Кирилл on 23.07.2016.
 */
const db = require("../mongo_database").MongoDatabase;
const util = require("../util");

function addClientAction(req, res, next) {
    console.log("addClientAction, params: ", req.params);

    var phone = decodeURIComponent(req.params.phone);
    var email = decodeURIComponent(req.params.email);
    
    db.insertMany("clients", [{phone: phone, email: email}], (err) => {
        if (err) {
            console.log("Error: when inserting a client: ", err.message, err.stack);
            util.renderView("add_client", {message: "Ошибка", msgType: "fail"}, res);
        } else {
            console.log("Client added");
            util.renderView("add_client", {message: "Клиент добавлен", msgType: "success"}, res);
        }
    });
}

module.exports = addClientAction;