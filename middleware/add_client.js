/**
 * Created by Кирилл on 23.07.2016.
 */
const util = require("../util");

function addClient(req, res, next) {
    console.log("addClient");
    util.renderView("add_client", {}, res);
}

module.exports = addClient;