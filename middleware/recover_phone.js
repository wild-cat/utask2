/**
 * Created by Кирилл on 23.07.2016.
 */
const util = require("../util");

function recoverPhone(req, res, next) {
    console.log("recoverPhone");
    util.renderView("recover_phone", {}, res);
}

module.exports = recoverPhone;