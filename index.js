/**
 * Created by kirill.vladykin on 07.07.2016.
 */
const PORT = 1002;

const express = require("express");
const app = express();
const middleware = require("./middleware");

app.get("/", middleware.addClient);
app.get("/phone/:phone/email/:email", middleware.addClientAction);
app.get("/recover", middleware.recoverPhone);
app.get("/recover/email/:email/key/:key", middleware.recoverPhoneAction);
app.use(express.static("public"));
app.all("*", middleware.pageNotFound);

app.listen(PORT, (err) => {
    if (err) {
        console.log(`Error: couldn't start server: ${err.message} ${err.stack}`);
        return;
    }
    console.log(`Server is listening on port ${PORT}`);
});
